# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Javier Vinal <fjvinal@gmail.com>, 2010, 2011, 2013, 2014, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-26 00:46+0000\n"
"PO-Revision-Date: 2016-01-04 12:34+0100\n"
"Last-Translator: Javier Vinal <fjvinal@gmail.com>\n"
"Language-Team: Spanish <l10n-kde-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: configwidget.cpp:28
#, kde-format
msgid "The selected path is empty."
msgstr "La ruta seleccionada está vacía."

#: configwidget.cpp:41
#, kde-format
msgid "The selected path contains valid Maildir folders."
msgstr "La ruta seleccionada contiene carpetas Maildir válidas."

#: configwidget.cpp:48
#, kde-format
msgid "The selected path is a valid Maildir."
msgstr "La ruta seleccionada es un Maildir válido."

#: configwidget.cpp:54
#, kde-format
msgid "The selected path does not exist yet, a new Maildir will be created."
msgstr "La ruta seleccionada no existe, se creará un nuevo Maildir."

#: configwidget.cpp:57
#, kde-format
msgid "The selected path does not exist."
msgstr "La ruta seleccionada no existe."

#: mixedmaildirresource.cpp:102 mixedmaildirresource.cpp:117
#: mixedmaildirresource.cpp:148 mixedmaildirresource.cpp:178
#: mixedmaildirresource.cpp:193 mixedmaildirresource.cpp:208
#: mixedmaildirresource.cpp:240 mixedmaildirresource.cpp:253
#: mixedmaildirresource.cpp:280 mixedmaildirresource.cpp:311
#: mixedmaildirresource.cpp:339
#, kde-format
msgctxt "@info:status"
msgid "Unusable configuration."
msgstr "Configuración no utilizable."

#: mixedmaildirresource.cpp:171
#, kde-format
msgctxt "@info:status"
msgid ""
"Item %1 belongs to invalid collection %2. Maybe it was deleted meanwhile?"
msgstr ""
"El elemento %1 pertenece a una colección no válida %2. ¿Quizás se borró "
"mientras tanto?"

#: mixedmaildirresource.cpp:202
#, kde-format
msgctxt "@info:status"
msgid "Synchronizing email folders"
msgstr "Sincronización de carpetas de correo"

#: mixedmaildirresource.cpp:217
#, kde-format
msgctxt "@info:status"
msgid "Synchronizing email folder %1"
msgstr "Sincronización de la carpeta de correo %1"

#: mixedmaildirresource.cpp:318
#, kde-format
msgctxt "@info:status"
msgid "Cannot move root maildir folder '%1'."
msgstr "No se puede mover la carpeta maildir raíz «%1»."

#: mixedmaildirresource.cpp:346
#, kde-format
msgid "Cannot delete top-level maildir folder '%1'."
msgstr "No se puede borrar la carpeta maildir raíz «%1»."

#: mixedmaildirresource.cpp:360
#, kde-format
msgctxt "@info:status"
msgid "Unable to create maildir '%1'."
msgstr "No es posible crear la carpeta de correo «%1»."

#: mixedmaildirresource.cpp:372
#, kde-format
msgctxt "@info:status"
msgid "No usable storage location configured."
msgstr "No está configurada ninguna ubicación de almacenamiento utilizable."

#. i18n: ectx: label, entry (Path), group (General)
#: mixedmaildirresource.kcfg:10
#, kde-format
msgid "Path to KMail mail folder"
msgstr "Ruta a la carpeta de correo de KMail"

#. i18n: ectx: label, entry (TopLevelIsContainer), group (General)
#: mixedmaildirresource.kcfg:14
#, kde-format
msgid ""
"Path points to a folder containing Maildirs instead of to a maildir itself."
msgstr ""
"La ruta apunta a una carpeta que contiene Maildirs en lugar de a un Maildir "
"en sí."

#. i18n: ectx: label, entry (ReadOnly), group (General)
#: mixedmaildirresource.kcfg:18
#, kde-format
msgid "Do not change the actual backend data."
msgstr "No modificar los datos reales del motor."

#: mixedmaildirstore.cpp:550
#, kde-format
msgctxt "@info:status"
msgid "Given folder name is empty"
msgstr "El nombre de la carpeta proporcionado está vacío"

#: mixedmaildirstore.cpp:585 mixedmaildirstore.cpp:604
#, kde-format
msgctxt "@info:status"
msgid "Folder %1 does not seem to be a valid email folder"
msgstr "La carpeta %1 no parece ser una carpeta válida de correo"

#: mixedmaildirstore.cpp:973
#, kde-format
msgctxt "@info:status"
msgid "Unhandled operation %1"
msgstr "Operación no manejada %1"

#: mixedmaildirstore.cpp:986 mixedmaildirstore.cpp:999
#: mixedmaildirstore.cpp:1006 mixedmaildirstore.cpp:1014
#: mixedmaildirstore.cpp:1025
#, kde-format
msgctxt "@info:status"
msgid "Cannot create folder %1 inside folder %2"
msgstr "No se puede crear la carpeta %1 dentro de la carpeta %2"

#: mixedmaildirstore.cpp:1053 mixedmaildirstore.cpp:1062
#: mixedmaildirstore.cpp:1070 mixedmaildirstore.cpp:1077
#, kde-format
msgctxt "@info:status"
msgid "Cannot remove folder %1 from folder %2"
msgstr "No se puede eliminar la carpeta %1 de la carpeta %2"

#: mixedmaildirstore.cpp:1113 mixedmaildirstore.cpp:1652
#, kde-format
msgctxt "@info:status"
msgid "Failed to load MBox folder %1"
msgstr "Ha fallado al cargar la carpeta MBox %1"

#: mixedmaildirstore.cpp:1177 mixedmaildirstore.cpp:1193
#: mixedmaildirstore.cpp:1228 mixedmaildirstore.cpp:1236
#, kde-format
msgctxt "@info:status"
msgid "Cannot rename folder %1"
msgstr "No se puede cambiar el nombre a la carpeta %1"

#: mixedmaildirstore.cpp:1297 mixedmaildirstore.cpp:1314
#: mixedmaildirstore.cpp:1335 mixedmaildirstore.cpp:1375
#: mixedmaildirstore.cpp:1387 mixedmaildirstore.cpp:1421
#, kde-format
msgctxt "@info:status"
msgid "Cannot move folder %1 from folder %2 to folder %3"
msgstr "No se puede mover la carpeta %1 de la carpeta %2 a la %3"

#: mixedmaildirstore.cpp:1463 mixedmaildirstore.cpp:1477
#: mixedmaildirstore.cpp:1502 mixedmaildirstore.cpp:1532
#, kde-format
msgctxt "@info:status"
msgid "Cannot add emails to folder %1"
msgstr "No se pueden añadir correos a la carpeta %1"

#: mixedmaildirstore.cpp:1555 mixedmaildirstore.cpp:1567
#: mixedmaildirstore.cpp:1581 mixedmaildirstore.cpp:1613
#, kde-format
msgctxt "@info:status"
msgid "Cannot remove emails from folder %1"
msgstr "No se pueden eliminar correos de la carpeta %1"

#: mixedmaildirstore.cpp:1679 mixedmaildirstore.cpp:1719
#, kde-format
msgctxt "@info:status"
msgid "Error while reading mails from folder %1"
msgstr "Error al leer correos de la carpeta %1"

#: mixedmaildirstore.cpp:1700
#, kde-format
msgctxt "@info:status"
msgid "Failed to load Maildirs folder %1"
msgstr "Ha fallado al cargar la carpeta Maildir %1"

#: mixedmaildirstore.cpp:1759 mixedmaildirstore.cpp:1771
#: mixedmaildirstore.cpp:1785 mixedmaildirstore.cpp:1819
#: mixedmaildirstore.cpp:1843
#, kde-format
msgctxt "@info:status"
msgid "Cannot modify emails in folder %1"
msgstr "No se pueden modificar correos en la carpeta %1"

#: mixedmaildirstore.cpp:1874
#, kde-format
msgctxt "@info:status"
msgid "Cannot modify emails in folder %1. %2"
msgstr "No se pueden modificar correos en la carpeta %1. %2"

#: mixedmaildirstore.cpp:1899 mixedmaildirstore.cpp:1930
#: mixedmaildirstore.cpp:1954
#, kde-format
msgctxt "@info:status"
msgid "Cannot move emails from folder %1"
msgstr "No se pueden mover correos de la carpeta %1"

#: mixedmaildirstore.cpp:1913 mixedmaildirstore.cpp:1941
#: mixedmaildirstore.cpp:1986 mixedmaildirstore.cpp:2010
#: mixedmaildirstore.cpp:2017 mixedmaildirstore.cpp:2096
#: mixedmaildirstore.cpp:2120
#, kde-format
msgctxt "@info:status"
msgid "Cannot move emails to folder %1"
msgstr "No se pueden mover correos a la carpeta %1"

#: mixedmaildirstore.cpp:1962 mixedmaildirstore.cpp:2062
#: mixedmaildirstore.cpp:2084
#, kde-format
msgctxt "@info:status"
msgid "Cannot move email from folder %1"
msgstr "No se puede mover correo de la carpeta %1"

#: mixedmaildirstore.cpp:2040 mixedmaildirstore.cpp:2145
#, kde-format
msgctxt "@info:status"
msgid "Cannot move email from folder %1 to folder %2"
msgstr "No se puede mover correo de la carpeta %1 a la carpeta %2"

#: mixedmaildirstore.cpp:2298
#, kde-format
msgctxt "@info:status"
msgid "Cannot move folder %1 into one of its own subfolder tree"
msgstr ""
"No se puede mover la carpeta %1 en una de su propio árbol de subcarpetas"

#: mixedmaildirstore.cpp:2310
#, kde-format
msgctxt "@info:status"
msgid "Cannot add email to folder %1 because there is no email content"
msgstr "No se puede añadir correo a la carpeta %1 porque no contiene correo"

#: mixedmaildirstore.cpp:2318
#, kde-format
msgctxt "@info:status"
msgid "Cannot modify email in folder %1 because there is no email content"
msgstr ""
"No se puede modificar correo en la carpeta %1 porque no contiene correo"

#. i18n: ectx: property (windowTitle), widget (QWidget, ConfigWidget)
#: settings.ui:15
#, kde-format
msgid "Mail Directory Settings"
msgstr "Preferencias del directorio de correo"

#. i18n: ectx: property (text), widget (QLabel, label)
#: settings.ui:21
#, kde-format
msgid "Select the folder containing the maildir information:"
msgstr "Seleccionar la carpeta que contiene la información de maildir:"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ReadOnly)
#: settings.ui:31
#, kde-format
msgid "Open in read-only mode"
msgstr "Abrir en modo de solo lectura"

#~ msgid "Select a KMail Mail folder"
#~ msgstr "Seleccionar carpeta de correo de KMail"
